type constraints

type user_type =
  | Int
  | Polymorphic of int
  | Arrow of user_type * user_type
  | Pair of user_type * user_type

val generate_contraints : Ast.term -> constraints
val print_constraints : constraints -> unit
val print_constraints_debug : constraints -> unit
val unification : constraints -> user_type option
val print_user_type : user_type -> unit
