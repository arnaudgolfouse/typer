module type OrderedType = sig
  type t
  type payload

  val compare : t -> t -> int
  val merge_payloads : payload -> payload -> payload option
end

module Union_find (O : OrderedType) : sig
  type element = O.t
  type t

  val empty : t

  (* returns `false` if the element already exists *)
  val add_element : t -> element -> O.payload -> t * bool

  (* returns the "payload" of both sets: the first one became the new payload. *)
  (* Returns `None` if merging payloads failed (returned `None`) *)
  val union : t -> element -> element -> t * (O.payload * O.payload) option
  val get_representant : t -> element -> element
  val get_payload : t -> element -> O.payload
  val get_roots : t -> element Seq.t
  val get_elements : t -> element -> element Seq.t
end
