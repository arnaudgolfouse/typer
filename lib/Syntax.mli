(* Machin's indices *)

open Ast

val parse_term : string -> term
val print_term : term -> unit
val print_debug_term : term -> unit
