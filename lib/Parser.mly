%token EOF
%token LAMBDA "λ"
%token LET    "let"
%token IN     "in"
%token EQUAL  "="
%token DOT    "."
%token COMMA  ","
%token LPAR   "("
%token RPAR   ")"
%token <int> INT
%token <string> VAR

%start <(Ast.term, string) result> top_level
%%

top_level:
	| t = term; EOF { Ok t }
	| EOF { Error "unexpected end of file" }
	;

term:
	| "λ"; p = VAR; "."; body = term { Ast.Lambda { parameter = p ; body } }
	| "let"; v = VAR; "="; expression = term; "in"; rest = term { Ast.Binding { variable = v ; expression ; rest } }
	| t = term_pair { t }
	;

term_pair:
	| t1 = term_pair; ","; t2 = term_application { Ast.Pair (t1, t2) }
	| t = term_application { t }
	;

term_application:
	| t1 = term_application; t2 = term_atom { Ast.Application (t1, t2) }
	| t = term_atom { t }
	;

term_atom:
	| v = VAR { Ast.Var v }
	| n = INT { Ast.Number n }
	| "("; t = term; ")" { t }
	;