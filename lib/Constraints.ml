open Ast

type inference_variable = int

type inference_type =
  | Variable of inference_variable
  | Arrow of inference_variable * inference_variable
  | Pair of inference_variable * inference_variable
  | Int

type constraints = inference_type list list

type user_type =
  | Int
  | Polymorphic of int
  | Arrow of user_type * user_type
  | Pair of user_type * user_type

module Constraint_context = Map.Make (String)

let generate_contraints (term : term) =
  let var_generate = ref 0 in
  let new_var () =
    var_generate := !var_generate + 1;
    !var_generate
  in
  (* ⟦ c ⊢ t : X ⟧ *)
  let rec aux context term var =
    match term with
    | Var x -> (
        let v = Constraint_context.find_opt x context in
        match v with
        | None -> failwith "unreachable"
        | Some v -> [ [ Variable v; Variable var ] ])
    | Number _ -> [ [ Int; Variable var ] ]
    | Lambda l ->
        let var1 = new_var () in
        let var2 = new_var () in
        let new_context = Constraint_context.add l.parameter var1 context in
        let new_c = aux new_context l.body var2 in
        (* ⟦ c ⊢ λx. t : X ⟧ ≜ X = X1 → X2 ∧ ⟦ c[x ↤ X1] ⊢ t : X2 ⟧ *)
        [ Variable var; Arrow (var1, var2) ] :: new_c
    | Application (t, u) ->
        let vart = new_var () in
        let ct = aux context t vart in
        let varu = new_var () in
        let cu = aux context u varu in
        (* ⟦ c ⊢ t u : X ⟧ ≜ Xt = Xu → X ∧ ⟦ c ⊢ t : Xt ⟧ ∧ ⟦ c ⊢ u : Xu ⟧ *)
        List.append ct ([ Variable vart; Arrow (varu, var) ] :: cu)
    | Binding b ->
        aux context
          (Application
             (Lambda { parameter = b.variable; body = b.rest }, b.expression))
          var
    | Pair (t1, t2) ->
        let var1 = new_var () in
        let c1 = aux context t1 var1 in
        let var2 = new_var () in
        let c2 = aux context t2 var2 in
        (* ⟦ c ⊢ t1, t2 : X ⟧ ≜ X = X1 × X2 ∧ ⟦ c ⊢ t1 : X1 ⟧ ∧ ⟦ c ⊢ t2 : X2 ⟧ *)
        List.append c1 ([ Variable var; Pair (var1, var2) ] :: c2)
  in
  aux Constraint_context.empty term 0

let print_inference_type t =
  match t with
  | Variable x -> Printf.printf "X%i" x
  | Arrow (x1, x2) -> Printf.printf "X%i → X%i" x1 x2
  | Pair (x1, x2) -> Printf.printf "X%i × X%i" x1 x2
  | Int -> print_string "int"

let rec print_constraints (cs : constraints) =
  let rec print_equality_list l =
    match l with
    | [] -> ()
    | x :: xs ->
        print_inference_type x;
        if xs != [] then print_string " = ";
        print_equality_list xs
  in
  match cs with
  | c :: cs ->
      print_equality_list c;
      if cs != [] then print_string " ∧ ";
      print_constraints cs
  | [] -> ()

let print_constraints_debug (cs : constraints) =
  let rec print_equality_list_debug l =
    match l with
    | [] -> ()
    | x :: xs ->
        print_inference_type x;
        if xs != [] then print_string ", ";
        print_equality_list_debug xs
  in
  List.iteri
    (fun i c ->
      let _ = if i != 0 then print_string ", " in
      print_equality_list_debug c)
    cs

let rec print_user_type t =
  let is_trivial t = match t with Polymorphic _ | Int -> true | _ -> false in
  match t with
  | Int -> print_string "int"
  | Polymorphic x -> (
      match x with
      | 0 -> print_string "α"
      | 1 -> print_string "β"
      | 2 -> print_string "γ"
      | 3 -> print_string "δ"
      | _ -> print_int x)
  | Arrow (t1, t2) ->
      if not (is_trivial t1) then print_string "(";
      print_user_type t1;
      if not (is_trivial t1) then print_string ")";
      print_string " → ";
      print_user_type t2
  | Pair (t1, t2) ->
      if not (is_trivial t1) then print_string "(";
      print_user_type t1;
      if not (is_trivial t1) then print_string ")";
      print_string " × ";
      print_user_type t2

let discriminant t =
  match t with Variable _ -> 0 | Arrow _ -> 1 | Pair _ -> 2 | Int -> 3

let compare_inference_type t1 t2 =
  match (t1, t2) with
  | Variable x1, Variable x2 -> compare x1 x2
  | Arrow (x11, x12), Arrow (x21, x22) | Pair (x11, x12), Pair (x21, x22) ->
      let c = compare x11 x21 in
      if c == 0 then compare x12 x22 else c
  | _, _ -> compare (discriminant t1) (discriminant t2)

module Sets = UnionFind.Union_find (struct
  type t = inference_type
  type payload = inference_type

  let compare = compare_inference_type

  let merge_payloads (t1 : inference_type) (t2 : inference_type) =
    let d1 = discriminant t1 in
    let d2 = discriminant t2 in
    if d1 == d2 || d2 == 0 then Some t1 else if d1 == 0 then Some t2 else None
end)

(* =========== *)
(* Unification *)
(* =========== *)

let ( let* ) = Option.bind

(*
   Merge t1, and t2, and then apply the rule:
      S(X1,X2,...) = S(Y1,Y2,...) = T1 = T2 = ...
      → S(X1,X2,...) = T1 = T2 = ...
        ∧ X1 = Y1 ∧ X2 = Y2 ∧ ...
     - merge t1, t2
     - get their structure representant
     - If they are not variables (and are the same !), run `merge_types` again on (X1, Y1), (X2, Y2), etc...
  
     Return `Error` if t1 and t2 are incompatible.
*)
let rec merge_types sets t1 t2 =
  let sets, representant =
    if t1 != t2 then Sets.union sets t1 t2 else (sets, Some (t1, t1))
  in
  match representant with
  | Some (Arrow (x1, x2), Arrow (y1, y2)) | Some (Pair (x1, x2), Pair (y1, y2))
    ->
      let* sets =
        if x1 != y1 then merge_types sets (Variable x1) (Variable y1)
        else Some sets
      in
      let* sets =
        if x2 != y2 then merge_types sets (Variable x2) (Variable y2)
        else Some sets
      in
      Some sets
  | None -> None
  | _ -> Some sets (* one of them is a variable *)

module IntMap = Map.Make (Int)

module Visited = Set.Make (struct
  type t = inference_type

  let compare = compare_inference_type
end)

(*
  Returns `None` if the main type is recursive.
*)
let find_main_type sets : user_type option =
  (* Remark: we also minimize the 'Polymorphic' indices *)
  let rec aux sets mapping visited t =
    match Visited.find_opt t visited with
    | Some _ -> None
    | None -> (
        let visited = Visited.add t visited in
        let s = Sets.get_payload sets t in
        match s with
        (* | None -> (None, mapping) *)
        | Variable x ->
            let real_x = IntMap.find_opt x mapping in
            let real_x, mapping =
              match real_x with
              | None ->
                  let next_int = IntMap.fold (fun _ _ i -> i + 1) mapping 0 in
                  (next_int, IntMap.add x next_int mapping)
              | Some x -> (x, mapping)
            in
            Some (Polymorphic real_x, mapping)
        | Arrow (x1, x2) ->
            let* left, mapping = aux sets mapping visited (Variable x1) in
            let* right, mapping = aux sets mapping visited (Variable x2) in
            Some (Arrow (left, right), mapping)
        | Pair (x1, x2) ->
            let* left, mapping = aux sets mapping visited (Variable x1) in
            let* right, mapping = aux sets mapping visited (Variable x2) in
            Some (Pair (left, right), mapping)
        | Int -> Some (Int, mapping))
  in
  Option.map
    (fun (t, _) -> t)
    (aux sets IntMap.empty Visited.empty (Variable 0))

let add_equality_constraint sets ts =
  let add_under_struct sets t =
    let sets =
      match t with
      | Variable _ -> sets
      | Arrow (x1, x2) | Pair (x1, x2) ->
          fst
            (Sets.add_element
               (fst (Sets.add_element sets (Variable x1) (Variable x1)))
               (Variable x2) (Variable x2))
      | Int -> fst (Sets.add_element sets Int Int)
    in
    fst (Sets.add_element sets t t)
  in
  (* unify variables in ts with t *)
  (* This will also recursively run the full algorithm ! *)
  let rec unify_with_t t ts sets =
    match ts with
    | t1 :: ts ->
        let sets = add_under_struct sets t1 in
        let* sets = merge_types sets t t1 in
        unify_with_t t ts sets
    | [] -> Some sets
  in
  let t = List.hd ts in
  unify_with_t t ts sets

(* Find the 'main' type of the root expression in `c` *)
let unification c =
  let rec build_sets l sets =
    match l with
    | [] -> Some sets
    | ts :: tail ->
        let* sets = add_equality_constraint sets ts in
        build_sets tail sets
  in

  let* all_sets = build_sets c Sets.empty in
  find_main_type all_sets
