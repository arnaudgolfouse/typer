module Syntax : module type of Syntax = struct
  include Syntax
end

module Constraints : module type of Constraints = struct
  include Constraints
end
