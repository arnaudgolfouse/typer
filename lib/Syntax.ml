open Ast

exception ParseError of string

module MyLexer = Lexer.Make (struct
  type 'a t = ('a, string) result

  let return = Result.ok
  let bind = Result.bind
  let fail s = Error s
  let on_refill _ = Ok ()
end)

let parse_term input =
  let next_token lexbuf =
    let res = MyLexer.token lexbuf in
    match res with Ok t -> t | Error e -> raise (ParseError e)
  in
  let lexbuf = Lexing.from_string input in
  let res = Parser.top_level next_token lexbuf in
  match res with Ok t -> t | Error e -> raise (ParseError e)

let rec print_debug_term (t : term) : unit =
  match t with
  | Var x -> Printf.printf "Var %s" x
  | Number x -> Printf.printf "Number %i" x
  | Application (t1, t2) ->
      print_string "Application(";
      print_debug_term t1;
      print_string ", ";
      print_debug_term t2;
      print_string ")"
  | Lambda l ->
      Printf.printf "Lambda { parameter = %s, body = " l.parameter;
      print_debug_term l.body;
      print_string " }"
  | Binding b ->
      Printf.printf "Binding { variable = %s, expression = " b.variable;
      print_debug_term b.expression;
      print_string ", rest = ";
      print_debug_term b.rest;
      print_string " }"
  | Pair (t1, t2) ->
      print_string "Pair (";
      print_debug_term t1;
      print_string ", ";
      print_debug_term t2;
      print_string ")"

let rec print_term (t : term) : unit =
  match t with
  | Var x -> print_string x
  | Number x -> print_int x
  | Application (t1, t2) ->
      let is_var = match t1 with Var _ -> true | _ -> false in
      if not is_var then print_string " (";
      print_term t1;
      if not is_var then print_string ")";
      print_string " ";
      print_term t2
  | Lambda l ->
      Printf.printf "λ %s. (" l.parameter;
      print_term l.body;
      print_string ")"
  | Binding b ->
      Printf.printf "let %s = " b.variable;
      print_term b.expression;
      print_string " in ";
      print_term b.rest
  | Pair (t1, t2) ->
      print_string "(";
      print_term t1;
      print_string ", ";
      print_term t2;
      print_string ")"
