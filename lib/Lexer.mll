{
open Parser

module Make (M : sig
    type 'a t
    val return: 'a -> 'a t
    val bind: 'a t -> ('a -> 'b t) -> 'b t
    val fail : string -> 'a t

    (* Set up lexbuf *)
    val on_refill : Lexing.lexbuf -> unit t
end)
= struct

let refill_handler k lexbuf =
    M.bind (M.on_refill lexbuf) (fun () -> k lexbuf)
}

refill {refill_handler}

rule token = parse
| eof
    { M.return EOF }
| [' ' '\t']
    { token lexbuf }
| "λ"
    { M.return LAMBDA }
| "let"
    { M.return LET }
| "in"
    { M.return IN }
| '.'
    { M.return DOT }
| '='
    { M.return EQUAL }
| ','
    { M.return COMMA }
| '('
    { M.return LPAR }
| ')'
    { M.return RPAR }
| ['0'-'9']+ as i
    { M.return (INT (int_of_string i)) }
| ['a'-'z' 'A'-'Z']+ as v
    { M.return (VAR v) }
| _
    { M.fail "unexpected character" }
{
end
}