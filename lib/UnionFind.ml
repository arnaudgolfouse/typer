module type OrderedType = sig
  type t
  type payload

  val compare : t -> t -> int
  val merge_payloads : payload -> payload -> payload option
end

module Union_find (O : OrderedType) = struct
  type element = O.t

  module O_Map = Map.Make (O)
  module O_Set = Set.Make (O)

  type union_set = {
    elem : element;
    payload : O.payload ref;
    parent : union_set option ref;
    rank : int ref;
  }

  let rec find set =
    match !(set.parent) with
    | None -> set
    | Some p ->
        let parent = find p in
        set.parent := Some parent;
        parent

  type t = { mapping : union_set O_Map.t; roots : O_Set.t }

  let empty = { mapping = O_Map.empty; roots = O_Set.empty }

  let add_element (uf : t) (elem : element) (payload : O.payload) : t * bool =
    let exists =
      match O_Map.find_opt elem uf.mapping with None -> false | Some _ -> true
    in
    if not exists then
      ( {
          mapping =
            O_Map.add elem
              {
                elem;
                payload = ref payload;
                parent = { contents = None };
                rank = { contents = 0 };
              }
              uf.mapping;
          roots = O_Set.add elem uf.roots;
        },
        true )
    else (uf, false)

  let union (uf : t) (e1 : element) (e2 : element) :
      t * (O.payload * O.payload) option =
    let set1 = O_Map.find e1 uf.mapping in
    let set2 = O_Map.find e2 uf.mapping in
    let root1 = find set1 in
    let root2 = find set2 in
    if root1 != root2 then (
      let payload = O.merge_payloads !(root1.payload) !(root2.payload) in
      match payload with
      | None -> (uf, None)
      | Some payload ->
          let other_payload =
            if payload == !(root1.payload) then root2.payload else root1.payload
          in
          let other_payload = !other_payload in
          let to_remove, new_root =
            if root1.rank < root2.rank then (root1, root2) else (root2, root1)
          in
          to_remove.parent := Some new_root;
          new_root.payload := payload;
          if to_remove.rank == new_root.rank then
            new_root.rank := !(new_root.rank) + 1;
          ( {
              mapping = uf.mapping;
              roots = O_Set.remove to_remove.elem uf.roots;
            },
            Some (payload, other_payload) ))
    else (uf, Some (!(root1.payload), !(root1.payload)))

  let get_representant (uf : t) (e : element) =
    let set = O_Map.find e uf.mapping in
    (find set).elem

  let get_payload (uf : t) (e : element) =
    let set = O_Map.find e uf.mapping in
    !((find set).payload)

  let get_roots uf = O_Set.to_seq uf.roots

  let get_elements uf e : element Seq.t =
    let set = O_Map.find e uf.mapping in
    let root = find set in
    Seq.map
      (fun (e, _) -> e)
      (O_Map.to_seq
         (O_Map.filter_map
            (fun e1 set1 ->
              let root1 = find set1 in
              if root == root1 then Some e1 else None)
            uf.mapping))
end
