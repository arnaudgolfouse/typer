type term =
  | Var of string
  | Number of int
  | Application of term * term
  | Lambda of { parameter : string; body : term }
  | Binding of { variable : string; expression : term; rest : term }
  | Pair of term * term