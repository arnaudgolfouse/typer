# λ-Typer

A very bare-bones typing inference engine for simply typed lambda-calculus (with pairs).

Close to the project for MPRI 2-4.

## To build and run

- install [opam](https://opam.ocaml.org/)
- In this directory, run
	```sh
	opam switch create . --deps-only
 	dune exec typer
	```