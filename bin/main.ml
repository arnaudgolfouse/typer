open Typer.Syntax
open Typer.Constraints

let process_input input =
  Printf.printf "input = \"%s\"\n" input;
  let term = parse_term input in
  let c = generate_contraints term in
  (* print_constraints c; *)
  (* print_newline (); *)
  let main_type = unification c in
  let _ =
    match main_type with
    | Some t ->
        print_string "  type is ";
        print_user_type t
    | None -> print_string "  Ill-typed"
  in
  print_newline ()

let () =
  print_endline "\nwell typed:\n===\n";
  let inputs =
    [
      "λ x . x";
      "let id = λ x . x in id (λ y. y)";
      "let id = λ x . x in id ( λ x. λ y. x )";
      "λ f. λ x. f x";
      "λ f. λ x. λ y. f x, f y";
      "λ f. λ x. λ y. f x, f y, f 0";
      (*
         X1  = X2 → X3
         X2  = X4
         X5  = X6 → X7
         X7  = X8 → X9
         X6  = X9
         X4  = X5 → X3
         X10 = X11 → X12
         X11 = X12
         X1  = X10 → X0
         =======================
         X1 = (X → X) → X
         X =  X6 → X8 → X6
         =======================
      *)
    ]
  in
  List.iter process_input inputs;
  print_endline "\nill-typed:\n===\n";
  let inputs = [ "let id = λ x. x in id id"; "( λ x. 0 x ) 1"; "0 1" ] in
  List.iter process_input inputs
